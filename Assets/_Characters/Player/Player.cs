﻿using UnityEngine;

// TODO consider re-wiring
using RPG.CameraUI;
using RPG.Core;

namespace RPG.Characters
{
    public class Player : MonoBehaviour, IDamageable
    {
        CameraRaycaster cameraRaycaster;

        [SerializeField] private float _maxHealthPoints = 100f;
        [SerializeField] private float _damagePerHit = 10f;
        [SerializeField] private float _minTimeBetweenHits = .5f;
        [SerializeField] private float _maxAttackRange = 2f;

        private GameObject _currentTarget;

        private float _currentHealthPoints;
        private float _lastHitTime;

        public float HealthAsPercentage
        {
            get { return _currentHealthPoints / _maxHealthPoints; }
        }

        private void Start()
        {
            RegisterForMouseClick();
            SetCurrentMaxHealth();
        }

        public void TakeDamage(float damage)
        {
            _currentHealthPoints = Mathf.Clamp(_currentHealthPoints - damage, 0f, _maxHealthPoints);
        }

        private void SetCurrentMaxHealth()
        {
            _currentHealthPoints = _maxHealthPoints;
        }

        private void RegisterForMouseClick()
        {
            cameraRaycaster = FindObjectOfType<CameraRaycaster>();
            cameraRaycaster.onMouseOverEnemy += OnMouseOverEnemy;
        }

        private void OnMouseOverEnemy(Enemy enemy)
        {
            if (Input.GetMouseButton(0) || IsTargetInRange(enemy))
            {
                AttackTarget(enemy);
            }
        }

        private void AttackTarget(Enemy enemy)
        {
            if (Time.time - _lastHitTime > _minTimeBetweenHits)
            {
                enemy.TakeDamage(_damagePerHit);
                _lastHitTime = Time.time;
            }
        }

        private bool IsTargetInRange(Enemy enemy)
        {
            float distanceToTarget = (enemy.gameObject.transform.position - transform.position).magnitude;
            return distanceToTarget <= _maxAttackRange;
        }
    }
}