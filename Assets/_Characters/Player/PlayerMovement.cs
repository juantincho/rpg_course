using RPG.CameraUI; // TODO consider rewiring 
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

namespace RPG.Characters
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(AICharacterControl))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class PlayerMovement : MonoBehaviour
    {
//    [SerializeField] private float _walkMoveStopRadius = 0.2f;
//    [SerializeField] private float _attackMoveStopRadius = 5f;
        private ThirdPersonCharacter _thirdPersonCharacter; // A reference to the ThirdPersonCharacter on the object
        private CameraRaycaster _cameraRaycaster;
        private AICharacterControl _aiCharacterControl;

        private Vector3 _clickPoint;

        private GameObject _walkTarget;

        private bool _isInDirectMode = false; // TODO consider making it static later


        private void Start()
        {
            _cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
            _thirdPersonCharacter = GetComponent<ThirdPersonCharacter>();
            _aiCharacterControl = GetComponent<AICharacterControl>();

            _walkTarget = new GameObject("walkTarget");

            _cameraRaycaster.OnMouseOverPotentiallyWalkable += OnMouseOverPotentiallyWalkable;
            _cameraRaycaster.onMouseOverEnemy += OnMouseOverEnemy;
        }

        private void OnMouseOverEnemy(Enemy enemy)
        {
            if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(1))
            {
                _aiCharacterControl.SetTarget(enemy.transform);
            }
        }

        private void OnMouseOverPotentiallyWalkable(Vector3 destination)
        {
            if (!Input.GetMouseButton(0)) return;
            _walkTarget.transform.position = destination;
            _aiCharacterControl.SetTarget(_walkTarget.transform);
        }


        // TODO make this get called again
        private void ProcessDirectMovement()
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            // calculate camera relative direction to move:
            Vector3 mCamForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 mMove = v * mCamForward + h * Camera.main.transform.right;

            _thirdPersonCharacter.Move(mMove, false, false);
        }
    }
}