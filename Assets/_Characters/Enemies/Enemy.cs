﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

//TODO consider re-wiring
using RPG.Core;
using RPG.Weapons;

namespace RPG.Characters
{
    public class Enemy : MonoBehaviour, IDamageable
    {
        [SerializeField] private float _maxHealthPoints = 100f;
        [SerializeField] private float _chaseRadius = 6f;

        [SerializeField] private float _attackRadius = 4f;
        [SerializeField] private float _damagePerShot = 9f;
        [SerializeField] private float _secondsBetweenShots = 0.5f;

        [SerializeField] private GameObject _projectileToUse;
        [SerializeField] private GameObject _projectileSocket;

        [SerializeField] private Vector3 _aimOffset = new Vector3(0, 1f, 0);


        private bool _isAtacking = false;

        private float _currentHealthPoints;

        private AICharacterControl _characterControl = null;
        private GameObject _player = null;

        public float HealthAsPercentage
        {
            get { return _currentHealthPoints / _maxHealthPoints; }
        }

        public void TakeDamage(float damage)
        {
            _currentHealthPoints = Mathf.Clamp(_currentHealthPoints - damage, 0f, _maxHealthPoints);

            if (_currentHealthPoints <= 0) Destroy(gameObject);
        }

        private void Start()
        {
            _characterControl = GetComponent<AICharacterControl>();
            _player = GameObject.FindGameObjectWithTag("Player");
            _currentHealthPoints = _maxHealthPoints;
        }

        private void Update()
        {
            float distanceToPlayer = Vector3.Distance(this.transform.position, _player.transform.position);

            if (distanceToPlayer <= _attackRadius && !_isAtacking)
            {
                _isAtacking = true;
                InvokeRepeating("FireProjectile", 0f, _secondsBetweenShots);
            }

            if (distanceToPlayer > _attackRadius)
            {
                _isAtacking = false;
                CancelInvoke();
            }

            if (distanceToPlayer <= _chaseRadius)
            {
                _characterControl.SetTarget(_player.transform);
            }
            else
            {
                _characterControl.SetTarget(this.transform);
            }
        }

        // TODO separate out Character firing logic
        private void FireProjectile()
        {
            GameObject newProjectile =
                Instantiate(_projectileToUse, _projectileSocket.transform.position, Quaternion.identity);

            Projectile projectileComponent = newProjectile.GetComponent<Projectile>();

            projectileComponent.DamageCaused = _damagePerShot;
            projectileComponent.Shooter = gameObject;

            Vector3 unitVectorToPlayer =
                (_player.transform.position + _aimOffset - _projectileSocket.transform.position).normalized;

            newProjectile.GetComponent<Rigidbody>().velocity = unitVectorToPlayer * projectileComponent.ProjectileSpeed;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _attackRadius);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, _chaseRadius);
        }
    }
}