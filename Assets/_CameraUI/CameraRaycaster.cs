﻿using UnityEngine;
using UnityEngine.EventSystems;
using RPG.Characters;

namespace RPG.CameraUI
{
    public class CameraRaycaster : MonoBehaviour // TODO rename cursor
    {
        [SerializeField] private const int PotentiallyWalkableLayerNumber = 9;
        readonly float _maxRaycastDepth = 100f; // Hard coded value


        [SerializeField] private Texture2D _walkCursor = null;
        [SerializeField] private Texture2D _enemyCursor = null;

        [SerializeField] private Vector2 _cursorHotspot = new Vector2(0, 0);

        // Setup delegates for broadcasting layer changes to other classes
        public delegate void OnMouseOverEnemy(Enemy enemy);

        public event OnMouseOverEnemy onMouseOverEnemy;

        public delegate void OnMouseOverTerrain(Vector3 destination);

        public event OnMouseOverTerrain OnMouseOverPotentiallyWalkable;

        private void Update()
        {
            // Check if pointer is over an interactable UI element
            if (EventSystem.current.IsPointerOverGameObject())
            {
                // Implement UI interaction
                return; // Stop looking for other objects
            }
            else
            {
                PerformRayCasts();
            }
        }

        private void PerformRayCasts()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            // Specify layer priorities below
            if (RaycastForEnemy(ray)) return;
            if (RaycastForPotentiallyWalkable(ray)) return;
        }

        private bool RaycastForEnemy(Ray ray)
        {
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo, _maxRaycastDepth);
            Enemy enemyHit = hitInfo.collider.gameObject.GetComponent<Enemy>();

            if (enemyHit)
            {
                Cursor.SetCursor(_enemyCursor, _cursorHotspot, CursorMode.Auto);
                onMouseOverEnemy(enemyHit);
                return true;
            }

            return false;
        }

        private bool RaycastForPotentiallyWalkable(Ray ray)
        {
            RaycastHit hitInfo;
            LayerMask potentiallyWalkableLayer = 1 << PotentiallyWalkableLayerNumber;

            bool potentiallyWalkableHit = Physics.Raycast(ray, out hitInfo, _maxRaycastDepth, potentiallyWalkableLayer);

            if (potentiallyWalkableHit)
            {
                Cursor.SetCursor(_walkCursor, _cursorHotspot, CursorMode.Auto);
                OnMouseOverPotentiallyWalkable(hitInfo.point);
            }

            return potentiallyWalkableHit;
        }
    }
}