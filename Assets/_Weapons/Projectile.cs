﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO consider re-wire
using RPG.Core;

namespace RPG.Weapons
{
    public class Projectile : MonoBehaviour
    {
        private const float DestroyDelay = 0.01f;

        [SerializeField] private float _projectileSpeed;

        public float DamageCaused { get; set; }

        [SerializeField] private GameObject _shooter;

        public float ProjectileSpeed
        {
            get { return _projectileSpeed; }
            set { _projectileSpeed = value; }
        }

        public GameObject Shooter
        {
            get { return _shooter; }
            set { _shooter = value; }
        }


        private void OnCollisionEnter(Collision collision)
        {
            if (_shooter.layer != collision.gameObject.layer)
            {
                InflictDamage(collision);
            }
        }

        private void InflictDamage(Collision collision)
        {
            Component damageableComponent = collision.gameObject.GetComponent(typeof(IDamageable));

            if (damageableComponent)
            {
                (damageableComponent as IDamageable).TakeDamage(DamageCaused);
            }

            Destroy(gameObject, DestroyDelay);
        }
    }
}